# [Glitter502](https://bitbucket.org/itichi/glitter502_public)
![Screenshot](http://i.imgur.com/MDo2rsy.jpg)

## UPDATE 2017

Please download **glitter502public/Glitter2015_2017.zip** and unzip it where you want your project to be.

It should compile for VS2015 AND VS2017 by using the solutions in their respective folder **Glitter2015** and **Glitter2017**.

When you run the software, it should ask for DLLs, you can find them in "vendors/libs/**{2015,2017}**/**{Debug,Release}**/bin"

When you are using Assimp, It may ask you to import the zlib.dll, you can find it in glitter502public/ZLIB/**{2015,2017}**/**{Debug,Release}**/"

## Summary

Glitter502 is based on [Glitter](https://github.com/Polytonic/Glitter) and is a dead simple boilerplate for OpenGL, intended as a starting point for the tutorials on [learnopengl.com](http://www.learnopengl.com), [open.gl](https://open.gl) and [opengl-tutorial.org](http://www.opengl-tutorial.org/). Glitter compiles and statically links every required library, so you can jump right into doing what you probably want: how to get started with OpenGL. Glitter502 implements some facilities for the lectures in order to accelerate the learning curve and has several exercises included.
Easy and advanced tutorials can also be found in [ogldev](http://ogldev.atspace.co.uk/).
## Getting Started
Glitter502 has a single dependency: [cmake](http://www.cmake.org/download/), which is used to generate platform-specific makefiles or project files. Start by cloning this repository, making sure to pass the `--recursive` flag to grab all the dependencies. If you forgot, then you can `git submodule update --init` instead.

In Windows, you can download [git for windows](https://git-for-windows.github.io/) in order to clone the repository.

```bash
git clone --recursive https://itichi@bitbucket.org/itichi/glitter502_public.git
cd glitter502_public/glitter502public
cd Build
```

Now generate a project file or makefile for your platform. If you want to use a particular IDE, make sure it is installed; don't forget to set the Start-Up Project in Visual Studio or the Target in Xcode.

```bash
# UNIX Makefile
cmake ..

# Mac OSX
cmake -G "Xcode" ..

# Microsoft Windows
cmake -G "Visual Studio 14" ..
cmake -G "Visual Studio 14 Win64" ..
...
```
For more informations you can refere to the document cmakeglitter502.docx in the folder.

If you compile and run, you should now be at the same point as the [Hello Window](http://www.learnopengl.com/#!Getting-started/Hello-Window) or [Context Creation](https://open.gl/context) sections of the tutorials. Open [main.cpp](https://github.com/Polytonic/Glitter/blob/master/Glitter/Sources/main.cpp) on your computer and start writing code!


## Material

files such as "INFOH502-Image synthesis and animation-slidesXX.pptx" are the learning material. You need to use them in order to understand the exercises theory and how everything works. During the exercises, if you share all several incomprehension, I will add some material in those slides. 


## Exercises

The exercises for Info-H-502 can be found in the INFOH502-XX-2016.docx files. You have to make only one report (It is a compilation of all the questions in red in those files). The solutions will be provided after your report.

The exercises are mainly inspired by [learnopengl.com](http://www.learnopengl.com) and [opengl-tutorial.org](http://www.opengl-tutorial.org/). A lot of high quality material can be found there in order to complement the theory and exercises sessions.

Those exercises and the report are meant to help you understand OpenGL and how to work with your project !


## Documentation
Many people overlook how frustrating it is to install dependencies, especially in environments lacking package managers or administrative privileges. For beginners, just getting set up properly can be a huge challenge. Glitter is meant to help you overcome that roadblock.

Glitter provides the most basic windowing example. It is a starting point, and tries very hard not to enforce any sort of directory structure. Feel free to edit the include paths in `CMakeLists.txt`. Glitter bundles most of the dependencies needed to implement a basic rendering engine. This includes:

| Functionality          | Library                                  |
| ---------------------- | ---------------------------------------- |
| Mesh Loading           | [assimp](https://github.com/assimp/assimp) |
| Physics                | [bullet](https://github.com/bulletphysics/bullet3) |
| OpenGL Function Loader | [glad](https://github.com/Dav1dde/glad)  |
| Windowing and Input    | [glfw](https://github.com/glfw/glfw)     |
| OpenGL Mathematics     | [glm](https://github.com/g-truc/glm)     |
| Texture Loading        | [stb](https://github.com/nothings/stb)   |

If you started the tutorials by installing [SDL](https://www.libsdl.org/), [GLEW](https://github.com/nigels-com/glew), or [SOIL](http://www.lonesock.net/soil.html), *stop*. The libraries bundled with Glitter supersede or are functional replacements for these libraries.

## Texture Loading

At the beginning of the "stb" header file, you can find the documentation on "How to use the library". It is not directly at the beginning, don't hesitate to scroll down.

## License
>The MIT License (MIT)

>Copyright (c) 2015 Kevin Fung

>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

GLuint createTriangleVAO(void) {
    GLfloat vertices[] = {
        // Positions
        1.0f, -1.0f, 0.0f,  // Bottom Right
        -1.0f, -1.0f, 0.0f, // Bottom Left
        0.0f,  1.0f, 0.0f   // Top 
    };

    // We generate a Vertex Array Object
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    // We generate a Vertex Buffer Object
    GLuint VBO;
    glGenBuffers(1, &VBO);

    // We bind the VBO (in order to send the data)
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // Store the data on the GPU. The VBO will point to the data
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // We bind the VAO (in order to configure the VBO)
    glBindVertexArray(VAO);
    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0); // Unbind VAO

    return VAO;
}

int main(int argc, char * argv[]) {
	[...]
	// Change Viewport
	int width, height;
	glfwGetFramebufferSize(mWindow, &width, &height);
	glViewport(0, 0, width, height);
	
    Shader triangleShader("tessellationTriangle.vert",
        "tessellationTriangle.frag",
        nullptr,
        "tessellationTriangle.tessC",
        "tessellationTriangle.tessE");
    triangleShader.compile();

    // Send a triangle to the GPU
    GLuint triangleVAO = createTriangleVAO();
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // wireframe

    // Rendering Loop
    while (glfwWindowShouldClose(mWindow) == false) {
        showFPS();

        // Background Fill Color
        glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        triangleShader.use();
        glBindVertexArray(triangleVAO);
        glDrawArrays(GL_PATCHES, 0, 3);
        glBindVertexArray(0);

        // Flip Buffers and Draw
        glfwSwapBuffers(mWindow);
        glfwPollEvents();
    }
	[...]
}


// tessellationTriangle.vert
#version 330 core
layout (location = 0) in vec3 position;

void main()
{
    gl_Position = vec4(position, 1.0);
}

// tessellationTriangle.tessC
#version 410 core
layout(vertices = 3) out;

void main(void)
{
    gl_TessLevelOuter[0] = 1.0;
    gl_TessLevelOuter[1] = 1.0;
    gl_TessLevelOuter[2] = 1.0;

    gl_TessLevelInner[0] = 1.0;

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}

// tessellationTriangle.tessE
#version 410 core

layout(triangles, fractional_even_spacing, ccw) in;

void main()
{
    vec3 bary = gl_TessCoord.xyz;
    gl_Position =
        gl_in[0].gl_Position * bary.x +
        gl_in[1].gl_Position * bary.y +
        gl_in[2].gl_Position * bary.z;
}

// tessellationTriangle.frag
#version 410 core

out vec4 color;
  
void main()
{
    color = vec4(vec3(0.0f, 0.5f, 0.0f), 1.0f);
}